 <style>
     .error{
         border-bottom: solid 1px #FF7264;
     }
</style>
  
   <script>
    function sendData(){
        
            var proceed = true;
            
            $('#first_name').removeClass('error');
            $('#email').removeClass('error');
            $('#textarea1').removeClass('error');
        
            if($('#first_name').val() == ''){
                $('#first_name').addClass('error');
                proceed = false;
            }
            var email_reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if($('#email').val() == '' || $('#email').attr('type')=='email' && !email_reg.test($.trim($('#email').val()))){
                $('#email').addClass('error');
                proceed = false;
            }
            if($('#textarea1').val() == ''){
                $('#textarea1').addClass('error');  //non rileva il val()
                proceed = false;
            }
				
            if(proceed == true)
               {
                $.ajax({
                                url: 'lib/form.php'
                                ,type: "POST"
                                ,data: {
                                    name: $('#first_name').val()
                                    ,surname: $('#last_name').val()
                                    ,mail: $('#email').val()
                                    ,message: $('#textarea1').val()
                                }
                                ,dataType: "json"   
                                ,error:OnError							
                                ,success:OnSentOK							
                            });
               }
				
			};
    
    function OnSentOK(){
        console.log($('#first_name').val());
        console.log($('#last_name').val());
        console.log($('#email').val());
        console.log($('#textarea1').val());
        
        $('#sxForm').hide();
        $('#mailSent').show();
    }   
       
    function OnError(jq)
            {
                switch(jq.status)
                {
                    case 400:
                        alert("Articolo non trovato")
                        break;
					case 404:
                        alert("Pagina non trovata")
                        break;
                    case 500:
                        alert("Errore 500")
                        break;
                    default:
                        alert(jq.statusText)
                }
                    
            };
</script>

<?php //CONTACT ?>

        <section id="contactSec" class="">
           <div id="formContainer" class="pdg0 android-more-section">
            
            <div class="row">
                <form id="contactForm" class="col s12 mdl-shadow--3dp" action="<?php echo($_SERVER['REQUEST_URI']); ?>" method="post">
                 <div id="formText" class="android-customized-section-text txtCtr mrgBtm100 pdg0">
            <div class="mdl-typography--font-light mdl-typography--display-1-color-contrast">Contattaci</div>
            <p class="mdl-typography--font-light">
              Contattaci per maggiori Informazioni o Controlla la disponibilità in Negozio
            </p>
            </div>
               
                  <div class="row floatLft" id="sxForm">
                    <div class="input-field col s6">
                      <input placeholder="" id="first_name" type="text" class="validate" name="name" onchange="classLabel()" value="">
                      <label class="nameLabel" for="first_name">Nome*</label>
                    </div>
                    <div class="input-field col s6">
                      <input id="last_name" placeholder="" type="text" class="validate"  name="surname" onchange="classLabel()" value="">
                      <label class="surnameLabel" for="last_name">Cognome</label>
                    </div>
                    <div class="input-field col s6">
                      <input id="email" placeholder="" type="email" class="validate" name="mail" onchange="classLabel()" value="">
                      <label class="mailLabel" for="email">Email*</label>
                    </div>
                    <div class="input-field col s6">
                        <textarea id="textarea1" class="materialize-textarea" name="message" onchange="classLabel()"></textarea>
                        <label class="messLabel" for="textarea1">Messaggio*</label>
                    </div>
                     <input type="hidden" name="sent" value="hiddenVal" />
                      <button type="button" onclick="sendData()" class="" id="formButton">Invia<img src="images/img/send.png"/></button>
                  </div>

                <div class="row floatLft txtCtr dNone" id="mailSent">
                    <p>Messaggio inviato correttamente! Riceverai una risposta al più presto.</p>
                    <br/>
                    <h6>CONSOLE STATION</h6>
                </div>
                                         
                </form>
                <!--<hr id="hrForm"/>-->
                <form id="avaibleForm" class="col s12 mdl-shadow--3dp">
                  <div class="row floatRht" id="dxForm">
                    <div id="radioBtns">
                        <p class="mrgBtm50 mrgTop30 txtUpp"><strong>Controlla disponibilità in Negozio</strong></p>
                        <div class="input-field col s6">
                          <input id="gameID" placeholder="Inserisci il nome del Gioco / Film" type="text" class="validate">
                        </div>
                        <div id="radioChoice">
                        <p class="floatLft mrgRht10">
                            <input id="cPS4" name="group1" type="radio" value="ps4" />
                          <label for="cPS4" class="radioInput">    
                            <span>PS4</span>
                          </label>
                        </p>
                        <p class="floatLft mrgRht10">
                          <input id="cONE" name="group1" type="radio" value="xone" />
                          <label for="cONE" class="radioInput">    
                            <span>XBOXONE</span>
                          </label>
                        </p>
                        <p class="floatLft mrgRht10">
                          <input id="cSWITCH" name="group1" type="radio" value="switch" />
                          <label for="cSWITCH" class="radioInput">
                            <span>SWITCH</span>
                          </label>
                        </p>
                        <p class="floatLft mrgRht10">
                          <input id="cPS3" name="group1" type="radio" value="ps3" />
                          <label for="cPS3" class="radioInput">    
                            <span>PS3</span>
                          </label>
                        </p>
                        <p class="floatLft mrgRht10">
                          <input id="c360" name="group1" type="radio" value="x360" />
                          <label for="c360" class="radioInput">    
                            <span>XBOX360</span>
                          </label>
                        </p>
                        <p class="floatLft mrgRht10">
                          <input id="c3DS" name="group1" type="radio" value="3ds" />
                          <label for="c3DS" class="radioInput">    
                            <span>3DS</span>
                          </label>
                        </p>
                        <p class="floatLft mrgRht10">
                          <input id="cPC" name="group1" type="radio" value="pc" />
                          <label for="cPC" class="radioInput">    
                            <span>PC</span>
                          </label>
                        </p>
                        <br/>
                        <p class="floatLft mrgRht10">
                          <input id="cDVD" name="group1" type="radio" value="dvd" />
                          <label for="cDVD" class="radioInput">    
                            <span>DVD</span>
                          </label>
                        </p>
                        <p class="floatLft">
                          <input id="cBR" name="group1" type="radio" value="br" />
                          <label for="cBR" class="radioInput">    
                            <span>BLURAY</span>
                          </label>
                        </p>
                        </div>
                    </div>
                    <p class="floatLft">
                      <input id="cNEW" type="checkbox" />
                      <label for="cNEW" class="checkInput">    
                        <span>Nuovo</span>
                      </label>
                    </p>
                    <p class="floatLft">
                        <input id="c2H" type="checkbox" />
                      <label for="c2H" class="checkInput">    
                        <span>Usato</span>
                      </label>
                    </p>
                    <button type="button" class="" id="avaibleButton">Verifica<img src="images/img/send.png"/></button>
                  </div>
                </form>
            </div>
         </div>
        </section>
        