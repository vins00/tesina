$('#zoomBtn').click(function() {  
  $('.zoom-btn-sm').toggleClass('scale-out');
  $('.zoom-fab').toggleClass('zoom-fab-active');
    
  if(!$('.fa-bars').hasClass('fa-bars-in') && !$('.fa-ellipsis-v').hasClass('fa-ellipsis-in')){
      $('.fa-bars').addClass('fa-bars-in');
      $('.fa-ellipsis-v').addClass('fa-ellipsis-in');
  }else if($('.fa-bars').hasClass('fa-bars-in') && $('.fa-ellipsis-v').hasClass('fa-ellipsis-in'))
  {
      $('.fa-bars').removeClass('fa-bars-in');
      $('.fa-ellipsis-v').removeClass('fa-ellipsis-in');
      $('.fa-bars').addClass('fa-bars-out');
      $('.fa-ellipsis-v').addClass('fa-ellipsis-out');
  }
    // close on outside click
$('#zoomBtn').mouseleave(function() {   
  $('.zoom-btn-sm').addClass('scale-out');
  $('.zoom-fab').removeClass('zoom-fab-active');
  $('.fa-bars').removeClass('fa-bars-in');
  $('.fa-ellipsis-v').removeClass('fa-ellipsis-in');    
  $('.fa-bars').addClass('fa-bars-out');
  $('.fa-ellipsis-v').addClass('fa-ellipsis-out');
});
  /*  
  if (!$('.zoom-card').hasClass('scale-out')) {
    $('.zoom-card').toggleClass('scale-out');
  }
});

$('.zoom-btn-sm').click(function() {
  var btn = $(this);
  var card = $('.zoom-card');

  if ($('.zoom-card').hasClass('scale-out')) {
    $('.zoom-card').toggleClass('scale-out');
  }
  if (btn.hasClass('zoom-btn-person')) {
    card.css('background-color', '#d32f2f');
  } else if (btn.hasClass('zoom-btn-doc')) {
    card.css('background-color', '#fbc02d');
  } else if (btn.hasClass('zoom-btn-tangram')) {
    card.css('background-color', '#388e3c');
  } else if (btn.hasClass('zoom-btn-report')) {
    card.css('background-color', '#1976d2');
  } else {
    card.css('background-color', '#7b1fa2');
  }
  */
});
