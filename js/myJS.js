var wdt = window.screen.width;

// PRELOAD

(function($){
  'use strict';
    $(window).on('load', function () {
        if ($(".preloader").length > 0)
        {
            $(".preloader").fadeOut("slow");
        }
    });
})(jQuery)

// NAV TOGGLE MOBILE

var mywindow = $('.android-content');
var mypos = mywindow.scrollTop();
var up = false;
var newscroll;
mywindow.scroll(function () {
    newscroll = mywindow.scrollTop();
    if (newscroll > mypos && !up && wdt < 992) {
        $('.android-header').stop().fadeOut();
        up = !up;
        console.log(up);
    } else if(newscroll < mypos && up && wdt < 992) {
        $('.android-header').stop().fadeIn();
        up = !up;
    }
    mypos = newscroll;
});

// SHOW/HIDE PRODUCTS

$(document).ready(function(){
    $('input[name="group2"]').click(function(){
        var inputValue = $(this).attr("value");
        console.log(inputValue);
        var targetBox = $("." + inputValue);
        $(".cont").not(targetBox).hide();
        $(targetBox).show();
        
        var cnsl = $("input[name='group2']:checked").val();
        
        $("input[name='group3']").click(function() {
        var test = $(this).val();
        $("#"+cnsl+test).children().removeClass('scale-in');
        $("#"+cnsl+test).children().addClass('scale-out');
        $("div.SH").hide();
        console.log(test);
        if($("div.SH").hide()){
           $("#"+cnsl+test).show();
           $("#"+cnsl+test).children().removeClass('scale-out');
           $("#"+cnsl+test).children().addClass('scale-in');
           }    
        });
    });
});


// FORM LABEL-OUT

function classLabel()
{
    var nameVal = $('#first_name').val();
    var surnameVal = $('#last_name').val();
    var mailVal = $('#email').val();
    var messVal = $('#textarea1').val();
    
    if(nameVal != '')
    {
        $('.nameLabel').addClass('labelOut');
    }else
    {
        $('.nameLabel').removeClass('labelOut');
    }
    if(surnameVal != '')
    {
        $('.surnameLabel').addClass('labelOut');
    }else
    {
        $('.surnameLabel').removeClass('labelOut');
    }
    if(mailVal != '')
    {
        $('.mailLabel').addClass('labelOut');
    }else
    {
        $('.mailLabel').removeClass('labelOut');
    }
    if(messVal != '')
    {
        $('.messLabel').addClass('labelOut');
    }else
    {
        $('.messLabel').removeClass('labelOut');
    }
}

// TOP BUTTON


$(document).ready(function(){
  $(".android-content").scroll(function() {
    if ($(".android-content").scrollTop() > 100 && wdt >= 992) {
      $("#topButton").addClass('scale-in');
      $("#topButton").removeClass('scale-out');
    } else {
      $("#topButton").removeClass('scale-in');    
      $("#topButton").addClass('scale-out');
    }
  });
});

// ACTIVE NAV

$(document).ready(function(){
  $(".android-content").scroll(function() {
//product      
    if ($(".android-content").scrollTop() > 800 && wdt >= 992) {
      $("a[href='#product']").addClass('navigation__linkCurrent');
    }else{$("a[href='#product']").removeClass('navigation__linkCurrent');}
//news      
    if($(".android-content").scrollTop() > 1780 && wdt >= 992){
      $("a[href='#product']").removeClass('navigation__linkCurrent');
      $("a[href='#news']").addClass('navigation__linkCurrent');
    }else{$("a[href='#news']").removeClass('navigation__linkCurrent');}
//services      
    if($(".android-content").scrollTop() > 3070 && wdt >= 992){
      $("a[href='#news']").removeClass('navigation__linkCurrent');
      $("a[href='#services']").addClass('navigation__linkCurrent');
    }else{$("a[href='#services']").removeClass('navigation__linkCurrent');} 
//review      
    if($(".android-content").scrollTop() > 4300 && wdt >= 992){
      $("a[href='#services']").removeClass('navigation__linkCurrent');
      $("a[href='#review']").addClass('navigation__linkCurrent');
    }else{$("a[href='#review']").removeClass('navigation__linkCurrent');}
//contact      
    if($(".android-content").scrollTop() > 5070 && wdt >= 992){
      $("a[href='#review']").removeClass('navigation__linkCurrent');
      $("a[href='#contact']").addClass('navigation__linkCurrent');
    }else{$("a[href='#contact']").removeClass('navigation__linkCurrent');}
//info      
    if($(".android-content").scrollTop() > 5520 && wdt >= 992){
      $("a[href='#contact']").removeClass('navigation__linkCurrent');
      $("a[href='#info']").addClass('navigation__linkCurrent');
    }else{$("a[href='#info']").removeClass('navigation__linkCurrent');}  
  });
});

// SERVICES POP UP

$(document).ready(function(){
   $(".android-content").scroll(function() {
    if($(".android-content").scrollTop() > 3000 && wdt >= 992) {
       $(".feature-cln").addClass('scale1');
       $(".android-customized-section-image").addClass('translate-image');
       }
    if($(".android-content").scrollTop() > 4270 && wdt < 992) {
       $(".feature-cln").addClass('scale1');
       }   
   });
});

// PARALLAX

$(document).ready(function(){
    $('.android-content').scroll(function(){
        var scrolling = $('.android-content').scrollTop();
        var position = scrolling * 0.2 - 300;
        $('#newsSec').css({
            'background-position': '160% -' + position + 'px'
        });
    })
/*    
    $('.android-content').scroll(function(){
        var scrolling = $('.android-content').scrollTop();
        var position = scrolling * 0.4 -250;
        $('#contactSec').css({
            'background-position': '0 -' + position + 'px'
        });
    })
*/    
    $('.android-content').scroll(function(){
        var scrolling = $('.android-content').scrollTop();
        var position = scrolling * 0.2 - 800;
        $('#reviewSec').css({
            'background-position': '-14% -' + position + 'px'
        });
    })
});

$(document).ready(function(){
    $('.android-content').scroll(function(){
        var scrolling = $('.android-content').scrollTop();
        var position = scrolling * 0.2 - 800;
        $('#newsSec').css({
            'background-position': '192% -' + position + 'px'
        });
    })
});
