    function getProducts(){
        var cVal = $('input[name=group2]:checked').val();
        var CONSOLE;
        
        switch(cVal)
       {
            case 'ALL':
               CONSOLE = 'all';
            break;
               
            case 'PS4':
               CONSOLE = 'ps4';
            break; 
               
            case 'ONE':
               CONSOLE = 'one';
            break; 
               
            case 'SWITCH':
               CONSOLE = 'switch';
            break;
               
            case '3DS':
               CONSOLE = '3ds';
            break;  
               
            case 'PC':
               CONSOLE = 'pc';
            break;   
               
            case 'MOVIE':
               CONSOLE = 'movie';
            break;
       }
        
            $.ajax({
                    url: 'dbPHPprod/'+ CONSOLE +'.php'
                    ,type: "GET"
                    ,dataType: "json"
                    ,success: OnSuccess  
                    ,error:OnError							
                });
        };

    
    function OnError(jq)
            {
                switch(jq.status)
                {
                    case 400:
                        alert("Articolo non trovato")
                        break;
					case 404:
                        alert("Pagina non trovata")
                        break;
                    case 500:
                        alert("Errore 500")
                        break;
                    default:
                        alert(jq.statusText)
                }
                    
            };
    
    function OnSuccess(data)
            {
                var target = $('input[name=group2]:checked').val();
                
                switch(target)
                   {
                        case 'ALL':
                           $('#ALLsoon').html(data.soon);
                           $('#ALLnews').html(data.news);
                           $('#ALLpromo').html(data.promo);
                           console.log('recupero_all');
                        break;

                        case 'PS4':
                           $('#PS4soon').html(data.soon);
                           $('#PS4news').html(data.news);
                           $('#PS4promo').html(data.promo);
                        break; 

                        case 'ONE':
                           $('#ONEsoon').html(data.soon);
                           $('#ONEnews').html(data.news);
                           $('#ONEpromo').html(data.promo);
                           console.log('recupero_one');
                        break; 

                        case 'SWITCH':
                           $('#SWITCHsoon').html(data.soon);
                           $('#SWITCHnews').html(data.news);
                           $('#SWITCHpromo').html(data.promo);
                           console.log('recupero_switch');
                        break;

                        case '3DS':
                           $('#3DSsoon').html(data.soon);
                           $('#3DSnews').html(data.news);
                           $('#3DSpromo').html(data.promo);
                           console.log('recupero_3ds');
                        break;  

                        case 'PC':
                           $('#PCsoon').html(data.soon);
                           $('#PCnews').html(data.news);
                           $('#PCpromo').html(data.promo);
                           console.log('recupero_pc');
                        break;   

                        case 'MOVIE':
                           $('#MOVIEsoon').html(data.soon);
                           $('#MOVIEnews').html(data.news);
                           $('#MOVIEpromo').html(data.promo);
                           console.log('recupero_movie');
                        break;
                   }

            };