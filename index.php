<?php 
require_once 'config2.php';
require_once 'lib/DbManager_mysqli.php';
?>
<?php require_once 'lib/function.php'; ?>
<?php require_once 'lib/PHPMailer/PHPMailer.php'; ?>
<?php require_once 'lib/PHPMailer/SMTP.php'; ?>


<?php $dbMan = new DbManager(DB_HOST,DB_NAME,DB_USER,DB_PASSWORD); ?>

<!doctype html>
<!--
  Material Design Lite
  Copyright 2015 Google Inc. All rights reserved.

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License
-->

<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html">
    <meta name="description" content="Sito Console Station v.DEMO">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>Console Station DEMO</title>

    <!-- Page styles -->
    <link rel="icon" href="favicon.png" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:100,200,300,400,500,600,700,800,900" rel="stylesheet">
    <link rel="stylesheet" href="css/icon.css">
    <link rel="stylesheet" href="css/material.min.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/myStyle.css">
    <link rel="stylesheet" href="css/newsStyle.css">
    <link rel="stylesheet" href="css/productsStyle.css">
    <link rel="stylesheet" href="css/floatingButton.css">
    <link rel="stylesheet" href="css/fontawesome-all.css">
    <link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/defaultStyle.css">
    <style>
    #view-source {
      position: fixed;
      display: block;
      right: 0;
      bottom: 0;
      margin-right: 40px;
      margin-bottom: 40px;
      z-index: 900;
    }
    </style>
  </head>
  <body class="pRel">
  
  <!-- Preload -->
  <div class="preloader">
      <img src="images/img/ps_preloader_Transp.gif" />
  </div>
  
  <!-- Facebook Plugin -->
   <div id="fb-root"></div>
      <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v3.1&appId=124974290853255&autoLogAppEvents=1';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
      </script>
      
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">

      <div class="android-header mdl-layout__header mdl-layout__header--waterfall">
        <div class="mdl-layout__header-row">
          <div class="android-title mdl-layout-title">
           <a href="#top">
               <img class="logoCS floatLft" src="images/img/logo.png"></a>
              <p class="titleCS fntBold floatLft mrg0"><a href="#top"><img class="logoIMG" src="images/img/cs.png"/></a></p>
          </div>
          <!-- Login -->
          <!-- <span id="login" class="mrgLft40">
              <a href="#" class="loginA">Accedi</a>
              <a href="#" class="signInA">Iscriviti</a>
          </span> -->
          <!-- Add spacer, to align navigation to the right in desktop -->
          <div class="android-header-spacer mdl-layout-spacer"></div>
          <div class="android-search-box mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right mdl-textfield--full-width">
            <label class="mdl-button mdl-js-button mdl-button--icon" for="search-field">
              <i class="material-icons">search</i>
            </label>
            <div class="mdl-textfield__expandable-holder">
              <input class="mdl-textfield__input" type="text" id="search-field">
            </div>
          </div>
          <!-- Navigation -->
          <div class="android-navigation-container">
            <nav id="mainNav" class="android-navigation mdl-navigation">
              <a class="mdl-navigation__link mdl-typography--text-uppercase nav-link" href="#product">Prodotti</a>
              <a class="mdl-navigation__link mdl-typography--text-uppercase" href="#news">Notizie</a>
              <a class="mdl-navigation__link mdl-typography--text-uppercase" href="#services">Servizi</a>
              <a class="mdl-navigation__link mdl-typography--text-uppercase" href="#review">In Evidenza</a>
              <a class="mdl-navigation__link mdl-typography--text-uppercase" href="#contact">Contatti</a>
              <a class="mdl-navigation__link mdl-typography--text-uppercase" href="#info">Info</a>
            </nav>
          </div>
          <div class="android-mobile-title mdl-layout-title">
            <p class="titleCS fntBold mrg0"><a href="#top"><img class="logoTitle" src="images/img/cs.png"/></a></p>
          </div>
          <span id="userIcon" class="android-search-box mdl-textfield mdl-js-textfield mdl-textfield--expandable mdl-textfield--floating-label mdl-textfield--align-right mdl-textfield--full-width mrg0"><i class="fas fa-user"></i></span>
          <button class="android-more-button mdl-button mdl-js-button mdl-button--icon mdl-js-ripple-effect" id="more-button">
            <i class="material-icons">more_vert</i>
          </button>
          <ul class="mdl-menu mdl-js-menu mdl-menu--bottom-right mdl-js-ripple-effect">
            <li class="mdl-menu__item">Il mio Account</li>
              <li class="mdl-menu__item"><a class="mdl-menu__item" href="">Wishlist</a></li>
            <li class="mdl-menu__item"><a class="mdl-menu__item" href="">Logout</a></li>
          </ul>
        </div>
      </div>

      <div class="android-drawer mdl-layout__drawer">
        <nav class="mdl-navigation">
          <a class="mdl-navigation__link" href="#product">Prodotti</a>
          <a class="mdl-navigation__link" href="#news">Notizie</a>
          <a class="mdl-navigation__link" href="#services">Servizi</a>
          <a class="mdl-navigation__link" href="#review">In Evidenza</a>
          <a class="mdl-navigation__link" href="#contact">Contatti</a>
          <a class="mdl-navigation__link" href="#info">Info</a>
          <div class="android-drawer-separator"></div>
          <span class="mdl-navigation__link">Account</span>
          <a class="mdl-navigation__link" href="">Il mio Account</a>
          <a class="mdl-navigation__link" href="">Wishlist</a>
          <a class="mdl-navigation__link" href="">Logout</a>
          <div class="android-drawer-separator"></div>
          <span class="mdl-navigation__link">Resources</span>
          <a class="mdl-navigation__link" href="">Official blog</a>
          <a class="mdl-navigation__link" href="">Console Station on Facebook</a>
          <a class="mdl-navigation__link" href="">Console Station on Twitter</a>
<!--          
          <div class="android-drawer-separator"></div>
          <span class="mdl-navigation__link">For developers</span>
          <a class="mdl-navigation__link" href="">App developer resources</a>
          <a class="mdl-navigation__link" href="">Android Open Source Project</a>
          <a class="mdl-navigation__link" href="">Android SDK</a>
-->          
        </nav>
      </div>
      
      <!-- FloatingButton -->
     <div class="zoom">
        <a class="zoom-fab zoom-btn-large" id="zoomBtn"><i class="fa fa-bars"></i><i class="fas fa-ellipsis-v"></i></a>
        <ul class="zoom-menu">
          <li><a class="zoom-fab zoom-btn-sm zoom-btn-product scale-transition scale-out" href="#product"><i class="fas fa-gamepad"></i></a></li>
          <li><a class="zoom-fab zoom-btn-sm zoom-btn-news scale-transition scale-out" href="#news"><i class="far fa-newspaper"></i></a></li>
          <li><a class="zoom-fab zoom-btn-sm zoom-btn-services scale-transition scale-out" href="#services"><i class="far fa-bell"></i></a></li>
          <li><a class="zoom-fab zoom-btn-sm zoom-btn-review scale-transition scale-out" href="#review"><i class="fas fa-align-left"></i></a></li>
          <li><a class="zoom-fab zoom-btn-sm zoom-btn-contact scale-transition scale-out" href="#contact"><i class="far fa-edit"></i></a></li>
          <li><a class="zoom-fab zoom-btn-sm zoom-btn-info scale-transition scale-out" href="#info"><i class="fas fa-info"></i></a></li>
        </ul>
<!--        <div class="zoom-card scale-transition scale-out">
          <ul class="zoom-card-content">
          <li>Content</li>
          <li>Content</li>
          <li>Content</li>
          <li>Content</li>
          <li>Content</li>
          </ul>
        </div>  -->
      </div>
       
       <!-- FloatingTopButton -->
       <div class="zoom2 scale-transition scale-out" id="topButton">
        <a href="#top" class="zoom-fab zoom-btn-large"><i class="fas fa-angle-up"></i></a>
      </div>
        
      <div class="android-content mdl-layout__content smoothScroll pRel" data-spy="scroll" data-target=".android-navigation">
        <a name="top"></a>
        <section class="android-be-together-section mdl-typography--text-center hgtAuto">
         <!--
          <div class="logo-font android-slogan">be together. not the same.</div>
          <div class="logo-font android-sub-slogan">welcome to android... be yourself. do your thing. see what's going on.</div>
          <div class="logo-font android-create-character">
            <a href=""><img src="images/andy.png"> create your android character</a>
          </div>

          <a href="#screens">
            <button class="android-fab mdl-button mdl-button--colored mdl-js-button mdl-button--fab mdl-js-ripple-effect">
              <i class="material-icons">expand_more</i>
            </button>
          </a> -->
          <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner" role="listbox">
              <!-- Slide One - Set the background image for this slide in the line below -->
               
$sSQL = "SELECT * FROM carousel ORDER BY slide_id DESC LIMIT 3";
$dbMan -> Esegui($sSQL);
$active = '';
$cc = 1;                
                
while($ret = $dbMan->Recupera()) { 
    if($cc == 1){$active = 'active';}else{$active = '';} ?>              
              <div class="carousel-item <?php echo($active) ?> carouselHgt carouselMyHgt" style="background-image: url('<?php echo($ret['img']) ?>')">
                <div class="carousel-caption d-md-block">
                  <h3><?php echo($ret['title']) ?></h3>
                  <hr class="carouselHR"/>
                  <p class="fntSz18"><?php echo($ret['description']) ?></p>
                </div>
              </div>
<?php $cc++; } ?>              
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
        </section>
        <a id="product"></a>
        
<?php include_once 'product.php'; ?>     
          
        <hr class="sectionHR"/>
        <a id="news"></a>
        
<?php include_once 'news.php'; ?>
               
        <a id="services"></a>
        
<?php include_once 'services.php'; ?>
               
        <a id="review"></a>
        
<?php include_once 'review.php'; ?>
               
        <a id="contact"></a>
        
<?php include_once 'contact.php'; ?>
               
        
<?php //include_once 'info.php'; ?>
       
        <footer class="android-footer mdl-mega-footer">
         <a id="info"></a>
          <div class="mdl-mega-footer--top-section pRel">
            <div class="mdl-mega-footer--left-section">
              <button id="fbButton" type="button" class="mdl-mega-footer--social-btn roundedSocial" onclick="window.open('https://www.facebook.com/consolestationsnc/')"><i class="fab fa-facebook-f"></i></button>
              &nbsp;
              <button id="twtButton" class="mdl-mega-footer--social-btn roundedSocial" onclick="window.open('https://twitter.com/?lang=it')"><i class="fab fa-twitter"></i></button>
              &nbsp;
            </div>
            <div id="fbLike" class="fb-like mrgLft20 mrgTop4" data-href="https://www.facebook.com/consolestationsnc/" data-layout="button" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
            <div id="subscribe" class="">
                <form id="subscribeForm">
                    <input class="floatLft" type="email" placeholder="Iscriviti alla Newsletter"/>
                    <button type="button" id="subButton" class="floatLft">Iscriviti</button>
                </form>
            </div>
<!--            
            <div class="mdl-mega-footer--right-section pAbs top2 rht4">
              <a class="mdl-typography--font-light" href="#top">
                Torna in Alto
                <i class="material-icons">expand_less</i>
              </a>
            </div>
-->            
          </div>

          <div class="mdl-mega-footer--middle-section">
            <p class="mdl-typography--font-light">VIA R. GIULIANI, 70/C ROSSO, 50141 FIRENZE FI · 055 428 9002 · <a class="mailSpan" href="#contact">@CONSOLESTATIONSNC</a></p>
            
            <table class="table-striped text-center">
            <tbody>
                  <tr>
                    <td class="pdgTop10 pdgBtm10 fntB500">LUNEDÌ</td>
                    <td>Chiuso</td>
                    <td>15.30 - 20</td>
                  </tr>
                  <tr>
                    <td  class="pdgTop10 pdgBtm10 fntB500">MARTEDÌ - SABATO</td>
                    <td>10 - 13</td>
                    <td>15.30 - 20</td>
                  </tr>
                  <tr>
                    <td  class="pdgTop10 pdgBtm10 fntB500">DOMENICA</td>
                    <td>Chiuso</td>
                    <td> </td>
                  </tr>
                </tbody>
           </table>
          </div>
          
          <div class="mapGoogle">
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23041.684495808746!2d11.233637971187557!3d43.789243643837736!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132a568faf29c739%3A0xfabeedef4bac2bd4!2sConsole+Station+Snc!5e0!3m2!1sit!2sit!4v1535056066209" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
           </div>

          <div class="mdl-mega-footer--bottom-section">
            <a class="android-link android-link-menu mdl-typography--font-light" id="version-dropdown">
              Versions
              <i class="material-icons">arrow_drop_up</i>
            </a>
            <ul class="mdl-menu mdl-js-menu mdl-menu--top-left mdl-js-ripple-effect">
              <li class="mdl-menu__item">5.0 Lollipop</li>
              <li class="mdl-menu__item">4.4 KitKat</li>
              <li class="mdl-menu__item">4.3 Jelly Bean</li>
              <li class="mdl-menu__item">Android History</li>
            </ul>
            <a class="android-link android-link-menu mdl-typography--font-light" id="developers-dropdown">
              For Developers
              <i class="material-icons">arrow_drop_up</i>
            </a>
            <ul class="mdl-menu mdl-js-menu mdl-menu--top-left mdl-js-ripple-effect">
              <li class="mdl-menu__item">App developer resources</li>
              <li class="mdl-menu__item">Android Open Source Project</li>
              <li class="mdl-menu__item">Android SDK</li>
              <li class="mdl-menu__item">Android for Work</li>
            </ul>
            <a class="android-link mdl-typography--font-light" href="">Blog</a>
            <a class="android-link mdl-typography--font-light" href="">Privacy Policy</a>
          </div>

        </footer>
      </div>
    </div>
    <!--
    <a href="https://github.com/google/material-design-lite/blob/mdl-1.x/templates/android-dot-com/" target="_blank" id="view-source" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-color--accent mdl-color-text--accent-contrast">View Source</a> -->
    
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <script src="js/floatingButton.js"></script>
    <script src="js/myJS.js"></script>
    <script src="https://code.getmdl.io/1.3.0/material.min.js"></script>
  </body>
</html>
