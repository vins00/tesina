-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Nov 05, 2018 at 04:20 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `db_console_station`
--

-- --------------------------------------------------------

--
-- Table structure for table `carousel`
--

CREATE TABLE `carousel` (
  `slide_id` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `carousel`
--

INSERT INTO `carousel` (`slide_id`, `img`, `title`, `description`) VALUES
(1, 'images/img/carousel/carousel1.jpg', 'Destiny 2: I Rinnegati', 'Fatti giustizia: esplora una nuova frontiera piena di nemici, alleati, misteri e tesori<br/>che aspettano di essere scoperti.'),
(2, 'images/img/carousel/carousel2.jpg', 'PS4 Pro', 'Vivi l\'emozione in 4K'),
(3, 'images/img/carousel/carousel3.jpg', 'Marvel\'s Spiderman', 'Scopri un nuovo capitolo nell’universo Marvel'),
(4, 'images/img/carousel/carousel12.jpg', 'Assassin\'s Creed Odyssey', 'Scegli tu la strada da percorrere');

-- --------------------------------------------------------

--
-- Table structure for table `console`
--

CREATE TABLE `console` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cons_prod_rel`
--

CREATE TABLE `cons_prod_rel` (
  `id_console` int(11) NOT NULL,
  `id_products` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `highlights`
--

CREATE TABLE `highlights` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `review` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `text` text NOT NULL,
  `image` varchar(500) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `price_disc` decimal(10,2) DEFAULT NULL,
  `description` text NOT NULL,
  `ps4` int(1) DEFAULT NULL,
  `xboxone` int(1) DEFAULT NULL,
  `switch` int(1) DEFAULT NULL,
  `3ds` int(1) DEFAULT NULL,
  `pc` int(1) DEFAULT NULL,
  `movie` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `image`, `date`, `price`, `price_disc`, `description`, `ps4`, `xboxone`, `switch`, `3ds`, `pc`, `movie`) VALUES
(1, 'destiny 2: i rinnegati', 'destiny_2_i_rinnegati.jpg', '2018-09-04', '0.00', '0.00', 'L\'Atollo è in preda all\'anarchia, e ora i criminali più pericolosi della galassia hanno organizzato un tentativo di fuga dalla Prigione degli Anziani. Tu e Cayde-6 siete incaricati di riportare l\'ordine, ma le cose non vanno come previsto. Dopo innumerevoli difficoltà, Cayde-6 paga un conto molto salato. Messa da parte l\'autorità dell\'Avanguardia, ti avventuri nell\'Atollo per farti giustizia da solo.', 1, 1, NULL, NULL, NULL, NULL),
(2, 'marverl\'s spiderman', 'marvels_spiderman.jpg', '2018-09-07', '0.00', NULL, 'Un nuovissimo gioco di Spider-Man\r\nMarvel\'s Spider-Man contiene tutte le acrobazie, le improvvisazioni e le ragnatele che hanno reso celebre l\'arrampicamuri, uno dei supereroi più famosi al mondo, introducendo allo stesso tempo elementi mai visti in un gioco di Spider-Man. Dalle sezioni di parkour all\'uso intelligente dell\'ambiente, fino ad arrivare a nuove meccaniche di combattimento e luoghi iconici, è uno Spider-Man mai visto prima.\r\n\r\nMarvel e Insomniac Games hanno fatto squadra per creare una storia di Spider-Man inedita ma sempre autentica. È uno Spider-Man come non se ne sono mai visti prima. Avremo a che fare con un Peter Parker navigato, abituato a combattere i peggiori criminali di New York. Allo stesso tempo, però, lotta per mantenere l\'equilibrio fra la sua caotica vita privata, la carriera e il destino di 9 milioni di newyorkesi che grava sulle sue spalle.\r\n\r\nDiventa Spider-Man\r\nPeter Parker ha già combattuto il crimine nei panni di Spider-Man per diversi anni e ha il pieno controllo del suo potentissimo senso di ragno, delle sue abilità acrobatiche e del suo nuovo costume.\r\n\r\nScopri una storia originale\r\nSi tratta di un universo di Spider-Man completamente inedito, con personaggi noti in ruoli inaspettati. Prendi il controllo di una doppia vita complessa, nei panni di un giovane con grandi poteri... e relazioni in evoluzione.\r\n\r\nEsplora un incredibile mondo aperto\r\nDondolati liberamente per le strade della New York dell\'universo Marvel e scopri situazioni ricche di azione, una trama epica e personaggi memorabili.', 1, NULL, NULL, NULL, NULL, NULL),
(3, 'assassin\'s creed odyssey', 'assassins_creed_odyssey.jpg', '2018-10-05', '0.00', NULL, 'Vivi una leggendaria Odissea e affronta un’epica avventura in un mondo dove ogni scelta conta. Condannato a morte dalla tua famiglia, imbarcati per un fantastico viaggio che, da mercenario reietto, farà di te un leggendario eroe Greco e svela i segreti che si celano dietro il tuo passato. Fatti strada in un mondo devastato dalla guerra e abitato da uomini e dei, dove montagne e mare si fondono. Incontra i personaggi più famosi dell’Antica Grecia e lascia la tua impronta su alcuni degli avvenimenti più fondamentali della storia della civiltà occidentale.\r\n\r\nIn Assassin’s Creed® Odyssey le scelte del giocatore rivestono un ruolo centrale come mai prima era accaduto nella saga, permettendo di scegliere il tipo di eroe che si vuole essere e di influenzare tutto il mondo di gioco. Sii arbitro del tuo destino attraverso i legami che crei con i personaggi e le scelte che intraprendi durante la tua Odissea. Personalizza il tuo equipaggiamento e padroneggia nuove abilità speciali, definendo il tuo stile di combattimento. Fatti strada per tutta la Grecia, combattendo feroci battaglie per mare e sulla terraferma per diventare un vero Eroe leggendario.\r\n\r\nDIVENTA UN LEGGENDARIO EROE GRECO – Per la prima volta nella serie di Assassin’s Creed, scegli se intraprendere la tua epica avventura nei panni di Alexios o di Kassandra. Durante il tuo viaggio potrai modificare il tuo equipaggiamento, migliorare le tue abilità e personalizzare la tua nave.\r\nIMMERGITI NELL’ANTICA GRECIA – Esplora un territorio totalmente incontaminato, dalle cime innevate delle montagne alle profondità del Mar Egeo, passando per le città simbolo dell’Epoca d’Oro dell’Antica Grecia. L’avventura sarà caratterizzata da incontri inaspettati con pittoreschi personaggi, da battaglie con crudeli mercenari e molto altro.\r\nSCEGLI TU LA STRADA DA PERCORRERE – Le tue scelte modificheranno il mondo che ti circonda con più di 30 ore di dialoghi e finali multipli. Vivi un mondo dinamico in costante evoluzione che cambia ad ogni tua decisione.\r\nCOMBATTI EPICHE BATTAGLIE – Dimostra le tue straordinarie abilità da guerriero e modifica l’andamento delle battaglie durante la Guerra del Peloponneso. Scendi in campo e fatti valere negli epici scontri fra Sparta e Atene che vedranno impegnati fino a 150 personaggi per schieramento.\r\nNAVIGA PER LE ACQUE DEL MAR EGEO – Scopri luoghi inesplorati, trova tesori segreti o combatti incredibili battaglie navali. Modifica la tua nave, migliora l’armamentario e recluta nuovi membri dotati di abilità speciali, adattando gli scontri in mare al tuo stile di combattimento.\r\nUNA TERRA FORGIATA DA MITI E LEGGENDE– Scopri un mondo ricco di miti e leggende. Tra antichi rituali e statue famose, interagisci con i personaggi più importanti della storia greca e scopri la verità che si cela dietro il mito.', 1, 1, NULL, NULL, NULL, NULL),
(4, 'spyro reignited trilogy', 'spyro_reignited_trilogy.jpg', '2018-11-13', '0.00', NULL, 'Il re degli sputafuoco è tornato!\r\n\r\nStesse scottature, stesso amore per il fuoco, ma ora tutto potenziato in HD. Spyro alza la temperature come mai prima d’ora in Spyro™ Reignited Trilogy. Riaccendi il fuoco con la trilogia originale, Spyro™ the Dragon, Spyro™ 2: Ripto\'s Rage! and Spyro™: Year of the Dragon. Esplora reami grandiosi, incontra tutti I personaggi originali e rivivi l’avventura nell’edizione completamente rimasterizzata. Perchè quando c’è un reame da salvare, c’è solo un drago da chiamare.\r\n\r\n*I minori di anni 18 devono avere il permesso di un genitore o di un tutore per effettuare l\'acquisto\r\n\r\nSpyro™ the Dragon\r\nGnasty Gnorc è ritornato dall’esilio, scatenando la sua perfida magia nel regno dei Draghi, intrappolando i Draghi nei cristalli con la sua armata di Gnorc. Spyro, con il suo amico Sparx the Dragonfly, è l’unico Drago rimasto che può attraversare il sei Homeworlds, liberare i Draghi e salvare il regno. Guida Spyro attraverso il regno dei Draghi, sbruciacchiando nemici super colorati, col suo soffio di fuoco e affrontando fantastiche avventure e risolvendo enigma lungo il percorso.\r\n\r\nSpyro™ 2: Ripto’s Rage!\r\nSpyro è andato nella terra di Avalar, per sconfiggere il terribilile stregone Ripto, che ha portato la Guerra nei mondi di Avalar. Ogni Homeworld è stato catturato da Ripto e dai suoi minions, e starà a Spyro sconfiggerlo e portare la pace nel regno di Avalar. Nella sua avventura, Spyro dovrà sconfiggere I nemici con le sue fiamme, i suoi super colpi e le sue mosse acrobatiche, facendosi largo tra i nemici e affrontando sfide impegnative, tra le quali lo scontro con dinosauri.\r\n\r\nSpyro™: Year of the Dragon\r\nI regni dei Draghi stanno celebrando “l’anno del Drago”’ quando una misteriosa figura appare per rubare le Uova dei Draghi. Sotto il controllo di malvagi stregoni, un esercito di Rhynocs sta sorvegliando le povere uova indifese nel Regno Dimenticato. Con i suoi vecchi amici, Spyro si getta alla ricerca delle uova disperse, sconfiggendo l’esercito di Rhynocs e I diabolici stregoni. Accendi il fuoco in questa avventura esplorando regni misteriosi e affrontando incredibili sfide, facendo trick sullo skateboard, o salendo su un ring di boxe.', 1, 1, NULL, NULL, NULL, NULL),
(5, 'fallout 76', 'fallout_76.jpg', '2018-11-14', '0.00', NULL, 'Bethesda Game Studios, acclamato creatore di Skyrim e Fallout 4, vi dà il benvenuto nel mondo di Fallout 76, il prequel online in cui ogni essere umano sopravvissuto è una persona vera. Lavorate insieme (oppure no!) per sopravvivere. Sotto la minaccia di un annientamento nucleare, avrete modo di interagire con il mondo di Fallout più grande e dinamico mai creato.\r\n\r\nGiornata della Rigenerazione, 2102. Venticinque anni dopo la caduta delle bombe, insieme agli altri abitanti del Vault (scelti tra i migliori della nazione), riemergerete finalmente nell\'America post-nucleare. Potrete giocare da soli o in compagnia esplorando, svolgendo missioni, costruendo e dando la caccia alle più temibili minacce della Zona Contaminata.\r\n\r\nÈ ora di emergere!\r\nIl multigiocatore fa finalmente la sua comparsa negli epici GDR a mondo aperto di Bethesda Game Studios. Creando il vostro personaggio con il sistema S.P.E.C.I.A.L. potrete forgiare il vostro destino in una nuova, selvaggia Zona Contaminata, ricca di centinaia di luoghi da visitare. Da soli o in compagnia di amici, una nuova grande avventura di Fallout vi aspetta!\r\n\r\nLo splendore delle montagne!\r\nGrafica, illuminazione e paesaggio sono frutto di una nuova tecnologia che darà vita a sei diverse aree della Virginia Occidentale. Dalle foreste dell\'Appalachia alle distese cremisi della Palude dei mirtilli, ogni area comporta qualche tipo di rischio e ricompensa. L\'America post-nucleare non è mai stata così bella!\r\n\r\nIl nuovo sogno americano!\r\nCon il nuovissimo Centro di Assistenza Mobile per la Produzione (C.A.M.P.) potrete costruire e produrre in ogni angolo del mondo. Il C.A.M.P. garantirà un riparo, rifornimenti e sicurezza. Sarà anche possibile impostare un luogo di scambio per commerciare con gli altri sopravvissuti. Ma attenzione... non tutti avranno intenzioni amichevoli!\r\n\r\nIl potere dell\'atomo!\r\nDa soli o con altri sopravvissuti, avrete la possibilità di sbloccare l’arma definitiva... i missili nucleari. La distruzione che ne consegue sbloccherà una zona di alto livello con risorse rare e di grande valore. Sceglierete di custodire o di scatenare il potere dell\'atomo? La scelta spetta a voi.', 1, 1, NULL, NULL, NULL, NULL),
(6, 'Red Dead Redemption 2', 'red_dead_redemption_2.jpg', '2018-10-26', '0.00', NULL, 'America, 1899. L\'era del selvaggio West è agli sgoccioli: la legge sta catturando le ultime bande di fuorilegge. Chi rifiuta di arrendersi viene ucciso senza pietà.\r\n\r\nDopo un colpo andato storto nella città di Blackwater, Arthur Morgan e la banda di Van der Linde sono costretti alla fuga. Con gli agenti federali e i migliori cacciatori di taglie alle costole, la banda deve rapinare, combattere e rubare per farsi strada e cercare di sopravvivere nel cuore di un\'America dura e selvaggia. Una serie di conflitti e divisioni rischia di mettere a repentaglio l\'unità del gruppo e Arthur si ritrova costretto a scegliere tra i suoi ideali e la lealtà nei confronti della banda che l\'ha cresciuto.\r\n\r\nDai creatori di Grand Theft Auto V e Red Dead Redemption, Red Dead Redemption 2 è una storia epica che ci mostra un\'America agli albori della modernità.', 1, 1, NULL, NULL, NULL, NULL),
(7, 'Call of Duty: Black Ops 4', 'black_ops_4.jpg', '2018-10-12', '0.00', NULL, 'Black Ops è tornato! Preparati a battaglie multigiocatore viscerali e \"con i piedi per terra\", all\'offerta Zombi più ricca di sempre con tre avventure distinte disponibili alla pubblicazione e a Blackout, una colossale esperienza in stile battle royale. Blackout vanta la mappa più grande nella storia di Call of Duty, sparatorie in stile Black Ops e personaggi, località e armi dalla storia di Black Ops.\r\n\r\nMULTIGIOCATORE TATTICO \"CON I PIEDI PER TERRA\"\r\nIl multigiocatore di Call of Duty®: Black Ops 4 fa un salto di qualità e offre l\'esperienza di combattimento più cruda e realistica di sempre, con un approccio improntato alla tattica e incentrato sulle scelte dei giocatori. Il gioco garantisce un nuovo livello di azione online con un vasto assortimento di armi, mappe e modalità inedite. Per la prima volta in assoluto, il multigiocatore è anche il cuore narrativo del gioco, permettendo ai giocatori di scoprire il ruolo e lo stile di gioco unico di ogni specialista. Con il ritorno del sistema Scegline 10 di Crea una classe, a cui si aggiungono le nuove attrezzature, l\'esperienza diventa più personalizzabile che mai e i giocatori possono scegliere come sviluppare gli specialisti. A tutto questo si aggiunge la possibilità di sbloccare serie di punti devastanti, che permettono ai giocatori di dominare da soli o in squadra.\r\n\r\nL\'ESPERIENZA ZOMBI PIÙ RICCA DI SEMPRE\r\nCall of Duty®: Black Ops 4 offrirà la modalità Zombi più ricca di sempre con tre esperienze distinte disponibili alla pubblicazione: IX, Viaggio disperato e Sangue dei morti. Con una nuova spumeggiante avventura e un nuovissimo cast di personaggi, la modalità Zombi di Call of Duty: Black Ops 4 si preannuncia carica di azione e di segreti che daranno filo da torcere anche ai fan più accaniti della community. \r\n \r\n\r\nL\'ESPERIENZA DI BLACK OPS AL LIMITE\r\nCon la nuova modalità Blackout, Black Ops offre un\'immensa esperienza in stile battle royale con gli inimitabili combattimenti di Black Ops e veicoli terrestri, navali e aerei nella mappa più grande nella storia di Call of Duty. Scegli i tuoi personaggi preferiti e combatti in inconfondibili località dell\'universo di Black Ops in una sfida di sopravvivenza estrema che unisce tutti i mondi di Black Ops.', 1, 1, NULL, NULL, NULL, NULL),
(8, 'Vampyr', 'vampyr.jpg', '2018-06-05', '0.00', NULL, 'Sangue e morte - Ti scorre nelle vene\r\nStrangolata dalla letale febbre spagnola, la Londra del 1918 è ammalata, spaventata e violenta. In questa città caotica e spettrale, i più deboli e disperati sono facili prede per i predatori più misteriosi del Regno Unito: i vampiri.\r\nDal caos, emerge una figura tormentata. Sei Jonathan E. Reid, un chirurgo militare di alto rango che viene trasformato in vampiro durante il suo ritorno dal fronte.\r\n\r\nUtilizza strumenti, armi e abilità sovrannaturali per affrontare un esercito di nemici, incluse creature brutali e astuti cacciatori di vampiri. Scegli con cura le tue vittime: uccidere può portare a gravi conseguenze...\r\n\r\nVIVI DA VAMPIRO – Combatti e manipola con abilità soprannaturali\r\nNUTRITI PER SOPRAVVIVERE – Dovrai essere il salvatore e il cacciatore', 1, 1, NULL, NULL, NULL, NULL),
(9, 'crash bandicoot n.sane trilogy', 'crash_bandicoot_nsane_trilogy.jpg', '2018-06-29', '0.00', NULL, 'Crash Bandicoot™, il tuo marsupiale preferito, è tornato! Più bello e scatenato che mai, è pronto a lanciarsi nelle danze nella collezione Trilogia N. Sane. Un Crash Bandicoot™ come non l\'avevi mai visto prima! Gira, salta e balla affrontando sfide e avventure epiche nei tre giochi che hanno dato inizio alla leggenda: Crash Bandicoot™, Crash Bandicoot™ 2: Il Ritorno di Cortex e Crash Bandicoot™: Teletrasportato. Rivivi i tuoi momenti preferiti in tutto lo splendore grafico dell\'HD completamente rimasterizzato per un divertimento in grande stile!\r\n\r\nCRASH BANDICOOT\r\nIl Dottor Neo Cortex vuole conquistare il mondo con un esercito di animali geneticamente modificati pronti a obbedire a ogni suo ordine. solo tu puoi aiutare crash a liberare gli animali, salvare la sua ragazza e mandare all\'aria i piani del Dottor Cortex!\r\n\r\nCRASH BANDICOOT 2: CORTEX STRIKES BACK\r\nIl diabolico Dottor Neo Cortex e tornato... per salvare il mondo? e ha chiesto aiuto proprio al suo arcinemico, Crash Bandicoot? e se fosse solo un astuto piano per attirare Cash in un nuovo, ignobile esperimento? riuscira Crash a vincere anche questa volta, o finira anche lui prigioniero nel serraglio di cortex?\r\n\r\nCRASH BANDICOOT 3: WARPED\r\nUna nuovissima avventura a spasso nel tempo! originalissimo gameplay all\'insegna dell\'azione piu sfrenata, tra immersioni subacquee ed esplorazioni in moto, in sella a un cucciolo di t-rex e in aereo, e molto altro ancora! piu azione. piu divertimento. piu rompicapi e livelli segreti.', 1, 1, NULL, NULL, NULL, NULL),
(10, 'far cry 5', 'far_cry_5.jpg', '2018-03-27', '0.00', '0.00', 'Far Cry è tornato in America nel nuovo episodio della pluripremiata serie.\r\nBenvenuto a Hope County, in Montana. Questo luogo idilliaco ospita una comunità di tranquilli amanti della libertà, ma anche una setta di fanatici apocalittici, conosciuto come il Progetto Eden’s Gate. Guidata dal carismatico profeta Joseph Seed e i suoi devoti seguaci, i Messaggeri, Eden’s Gate si è lentamente inserita in ogni aspetto della vita quotidiana di questa cittadina un tempo tranquilla. Ma quando il tuo arrivo in città incita la setta a prendere il controllo della regione con la violenza, dovrai ribellarti e guidare la resistenza per liberare una comunità sotto assedio.\r\nEsplora liberamente i fiumi, le terre e i cieli di Hope County con la più vasta gamma di armi e veicoli personalizzabili mai apparsa prima in un gioco di Far Cry. Diventa l’eroe della storia in uno scenario emozionante, che reagirà a ogni tua azione e dove i luoghi che scoprirai e gli alleati che incontrerai modelleranno la tua storia in modo assolutamente imprevedibile.\r\nCaratteristiche principali:\r\nGUIDA LA RESISTENZA CONTRO UNA SETTA DI FANATICI\r\nBloccato in un territorio ostile, dovrai trovare la forza nella comunità che ti circonda per guidare la resistenza contro una setta che intende prendere il controllo di Hope County, in Montana.\r\n•	Recluta Mercenari e sfrutta le abilità di un massimo di 3 alleati alla volta per cambiare le carte in tavola del Progetto Eden’s Gate.\r\n•	Comanda alcune Bestie Feroci altamente specializzate, animali che potrai controllare per supportare qualsiasi stile di gioco.\r\n•	Alleati con un amico mercenario per eliminare la setta in un’esperienza in cooperativa per due giocatori che si estende per tutta la campagna.\r\n\r\nCREA IL TUO PERCORSO:\r\nAvrai la massima libertà di andare in qualsiasi direzione: sarai tu a decidere dove, quando e come.\r\n•	Dal tuo arrivo a Hope County, avrai la libertà di affrontare lo scenario come preferisci.\r\n•	Per la prima volta in Far Cry, sei tu l’eroe della storia: scegli e personalizza il tuo personaggio.\r\n•	Crea nuove esperienze di Far Cry davvero folli e uniche con l’editor delle Mappe, per divertirti con o contro gli amici!\r\n\r\nUN MONDO IN EVOLUZIONE:\r\nScegli come contrastare Joseph Seed e i suoi fanatici seguaci in uno scenario dinamico e liberamente esplorabile, che si adatta e reagisce alle tue scelte.\r\n•	Man mano che progredirai, l’indicatore della Resistenza evolverà lo scenario in modo significativo, con ogni tua azione che ti farà proseguire nella storia.\r\n•	Ostacola i piani apocalittici della setta e trascina i Messaggeri in alcuni scontri epici in uno scenario liberamente esplorabile.\r\n•	Le tue scelte influenzeranno il mondo circostante e il caos derivante darà vita a nuove opportunità di gioco.\r\n\r\nSTRUMENTI DINAMICI:\r\n•	Sfreccia attraverso questo scenario a bordo di alcuni celebri veicoli americani che potrai personalizzare, da muscle car a grossi camion, fino a trattori e ATV.\r\n•	Comanda un aereo per affrontare le forze della setta in epici duelli aerei o con bombardamenti strategici.\r\n•	Scopri le ricchezze naturali del selvaggio Montana, andando a caccia, pescando ed esplorandone ogni anfratto.', 1, 1, NULL, NULL, NULL, NULL),
(11, 'jurassic world evolution', 'jurassic_world_evolution.jpg', '2018-07-03', '0.00', '0.00', 'Jurassic World Evolution è un simulatore strategico che ti permette di costruire, gestire e divertirti con il tuo personale Jurassic World. Assumi il controllo delle operazioni nel leggendario arcipelago Muertas e riporta la bellezza, la maestosità e il pericolo derivante dalla ricomparsa dei dinosauri.\r\nCostruisci a fini scientifici, di divertimento o di sicurezza all’interno di un mondo incerto dove la vita riesce sempre a trovare una via per emergere.\r\n\r\nRivivi il franchise come al cinema: entra nel vivo del franchise Jurassic World e ricostruisci il tour turistico più esaltante di sempre.\r\nControlla ogni dettaglio: sfrutta gli strumenti di management più tecnici o affronta sfide su terra o in aria. Espandi le isole e scegli il tuo viaggio.\r\nInteragisci con i dinosauri: i dinosauri reagiscono e vivono il mondo attorno a loro. Personalizzali e guadagna grazie a loro fondi per la tua ricerca sul DNA.', 1, 1, NULL, NULL, NULL, NULL),
(12, 'the crew 2', 'the_crew_2.jpg', '2018-06-29', '0.00', NULL, 'L\'iterazione più recente nel rivoluzionario franchise The Crew, THE CREW 2 cattura lo spirito dei motori americani in uno dei mondi aperti più emozionanti mai creati. Mettete alla prova le vostre abilità in una vasta gamma di discipline con auto, moto, barche e aerei. La guida raggiunge nuove vette, letteralmente: gli interi Stati Uniti d\'America sono a disposizione per competizioni, esplorazione e libertà senza limiti. Registrate ogni momento glorioso e condividetelo con il mondo: la fama vi attende!\r\n\r\nBenvenuti a Motornation, una splendida ed enorme nazione costruita appositamente per gareggiare: avete a disposizione tutti gli Stati Uniti d\'America. Esplorate liberamente terra, mare e cielo. Da costa a costa, i piloti di strada e professionisti, gli amanti dello sterrato e dello stile libero possono sfidarsi in ogni tipo di disciplina. Unitevi a loro in gare al cardiopalma e condividete i momenti gloriosi con il mondo. THE CREW 2 vi dà la possibilità di dimostrare il vostro talento e di diventare campioni delle corse di ogni tipo.\r\n\r\nUnitevi a quattro diverse famiglie motoristiche: piloti di strada e professionisti, esperti di fuoristrada e giocatori in stile libero. Vi coinvolgeranno con nuovi veicoli e scoprirete la loro cultura e le discipline. Tramite competizioni e incontri casuali, trovate e affinate il vostro stile personale, ottenete e personalizzate i veicoli dei vostri sogni, metteteli in bella mostra nel QG e diventate famosi nella scena motoristica americana.\r\n\r\nBENVENUTI A MOTORNATION – AFFRONTATE LA SFIDA A TERRA, IN ACQUA E NEI CIELI\r\nSpingetevi oltre i limiti e affrontate nuove esperienze in luoghi iconici. Volate e volteggiate attraverso la nebbia e le nuvole sopra le Montagne Rocciose innevate, consumate le gomme nei vicoli di New York, navigate lungo il Mississippi ed esplorate ogni centimetro del Grand Canyon. Macchine da sogno, alcune tra le moto americane più iconiche, aerei rapidi e motoscafi: le opportunità di divertimento e sfida negli Stati Uniti sono illimitate.\r\n\r\nDIVENTATE CAMPIONI\r\nUnitevi a quattro diverse famiglie motoristiche: piloti di strada e professionisti, esperti di fuoristrada e amanti del freestyle. Vi coinvolgeranno con nuovi veicoli e scoprirete la loro cultura e le discipline. Tramite competizioni e incontri casuali, trovate e affinate il vostro stile personale, ottenete e personalizzate i veicoli dei vostri sogni, metteteli in bella mostra nel QG e diventate famosi nella scena motoristica americana.\r\n\r\nCONDIVIDETE E BRILLATE IN UN MONDO CONNESSO\r\nIl mondo di The Crew® 2 è alimentato dalla voglia di condividere i risultati personali e i momenti unici con amici e altri giocatori: infrangete i record e diventate dei pionieri! Ogni volta che otterrete un risultato, questo sarà salvato come una nuova sfida per gli altri utenti, mentre voi sarete incoraggiati a superare i risultati degli altri giocatori. Catturate e condividete tutti i momenti migliori con un solo comando.', 1, 1, NULL, NULL, NULL, NULL),
(13, 'god of war', 'god_of_war.jpg', '2018-04-20', '0.00', NULL, 'Lasciatosi alle spalle la sua sete di vendetta verso gli dèi dell\'Olimpo, Kratos ora vive nella terra delle divinità e dei mostri norreni.\r\n\r\nÈ in questo mondo ostile e spietato che dovrà combattere per la sopravvivenza e insegnare a suo figlio non solo a fare lo stesso, ma anche a evitare di ripetere gli stessi errori fatali del Fantasma di Sparta...\r\n\r\nQuesta sorprendente rielaborazione di God of War contiene tutti gli elementi caratteristici dell\'iconica serie (combattimenti brutali, scontri epici con i boss e dimensioni mozzafiato) uniti a una narrazione intensa e commovente che getta una nuova luce sul mondo di Kratos.\r\n\r\nUna nuova intensa narrazione\r\nNella sua nuova veste di mentore e protettore di un figlio intenzionato a conquistare la stima del padre, Kratos ha davanti a sé l\'inaspettata opportunità di imparare a gestire la rabbia che lo ha sempre caratterizzato. Interrogandosi sull\'eredità oscura trasmessa al figlio, spera di poter rimediare alle mancanze e agli orrori del proprio passato.\r\n\r\nUn mondo ancora più oscuro\r\nImmerso in foreste, montagne e regni incontaminati tipici del folklore nordico, dovrai districarti in una nuova e pericolosa terra corredata da un pantheon di creature, mostri e dèi.\r\n\r\nFeroci combattimenti\r\nCon una intima visuale OTS (sopra la spalla), che porta l\'azione più vicina che mai, il combattimento è in primo piano, frenetico e duro. La potente ascia magica di Kratos assume una doppia funzione di arma brutale e di versatile strumento per l\'esplorazione. Non abbassare mai la guardia.', 1, NULL, NULL, NULL, NULL, NULL),
(14, 'Anthem', 'anthem.jpg', '2019-02-22', '0.00', NULL, 'In un mondo lasciato incompiuto dagli dei, un\'oscura fazione minaccia il resto dell\'umanità. L\'unico ostacolo che si frappone tra questi criminali e l\'antica tecnologia a cui ambiscono sono gli Specialisti.\r\nGioca con altri tre compagni e assembla armature tecnologiche forgiate per la battaglia e dotate di poteri unici. Esplora immense rovine, combatti contro nemici letali e fai tuoi manufatti soprannaturali. A ogni missione, tu e la tua armatura diventerete più potenti. Affronta i pericoli di un mondo in continua evoluzione. Uniti contro il male. Uniti nel trionfo.\r\n\r\nAnthem™ ti invita a scoprire un GDR d\'azione cooperativo ambientato in un mondo nuovo e misterioso creato da BioWare™ e EA.\r\n\r\nEroi che combattono insieme per la vittoria: alla base di Anthem c\'è un\'esperienza social vissuta in connessione. Unisciti ad altri tre compagni e vivi avventure cooperative che premiano sia il lavoro di squadra sia le abilità individuali. Lo strale, ossia l\'armatura scelta da ciascun giocatore, determinerà il suo contributo e il ruolo strategico offerto all\'interno della squadra. Parti per una spedizione o proponi un contratto per farti aiutare dagli amici nelle sfide più difficili. Nel corso dell\'esplorazione, scoprirai una storia avvincente, piena di personaggi unici e memorabili. Un matchmaking fluido e intelligente consente di trovare rapidamente e con facilità altri giocatori con cui condividere le proprie avventure.\r\nScegli tra un arsenale di armature personalizzabili: ogni volta che esplorerai il mondo nei panni di uno Specialista, avrai accesso alla tua collezione e potrai selezionare un potente strale, un\'armatura progettata per offrire al pilota incredibili capacità offensive e difensive. Usa armi imponenti e poteri devastanti. Incassa senza batter ciglio colpi in grado di spezzare le ossa. Muoviti a velocità incredibili, sfreccia nei cieli o tuffati in tutta sicurezza negli abissi dei mari. Durante il gioco, migliorerai costantemente sia le tue abilità come pilota di strali sia le capacità trasversali e di combattimento della tua armatura.\r\nPersonalizza gli strali per dimostrare i tuoi progressi: salendo di livello e sconfiggendo potenti avversari, raccoglierai bottini e completerai traguardi nel gioco, sbloccando incredibili opzioni di personalizzazione, capaci di trasformare gli strali in testimonianze visive della tua abilità e delle tue imprese.\r\nEsplora un mondo di pericoli e misteri in continua evoluzione: Frostbite dà vita allo spettacolare e mutevole open world di Anthem, con condizioni meteorologiche, pericoli e nemici imprevedibili. Anthem è una live service experience: cambierà e crescerà nel corso del tempo, introducendo nuove storie, sfide ed eventi. Questo mondo affascinante e letale offrirà sfide strategiche a ogni nuova spedizione che affronterai con la tua squadra di Specialisti.', 1, 1, NULL, NULL, NULL, NULL),
(15, 'Darksiders 3', 'darksiders_3.jpg', '2018-11-27', '0.00', NULL, 'In Darksiders III i giocatori assumono il ruolo di FURY, una strega che vuole sbarazzarsi dei \"Sette Peccati Capitali\".\r\nFURY ha una forma che si evolve attraverso il gameplay e con essa, i suoi poteri e le sue armi. L\'ampio mondo di gioco di Darksiders III è presentato come un pianeta Terra aperto, vivente, a forma libera, dilapidato dalla guerra e dal degrado e invaso dalla natura.\r\nFURY ripercorrerà i vari ambienti, combattendo le creature di altri mondi e sbloccando diversi enigmi mentre avanza nella storia di Darksiders.\r\n\r\nPotere apocalittico: Scatena l\'ira della guerra, combinando attacchi brutali e capacità soprannaturali per eliminare tutti quelli che si mettono sulla tua strada\r\nArsenale estremo: scopri un arsenale devastante di angeliche, demoniache e terrestri armi e scatena distruzione\r\nEpic Quest: Battaglia attraverso le terre desolate e le prigioni sotterranee infestate dai demoni\r\nProgressione personaggio: potenzia le armi, sblocca nuove abilità e personalizza il tuo stile di gioco.', 1, 1, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carousel`
--
ALTER TABLE `carousel`
  ADD PRIMARY KEY (`slide_id`);

--
-- Indexes for table `console`
--
ALTER TABLE `console`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cons_prod_rel`
--
ALTER TABLE `cons_prod_rel`
  ADD PRIMARY KEY (`id_console`,`id_products`);

--
-- Indexes for table `highlights`
--
ALTER TABLE `highlights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carousel`
--
ALTER TABLE `carousel`
  MODIFY `slide_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `console`
--
ALTER TABLE `console`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `highlights`
--
ALTER TABLE `highlights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
