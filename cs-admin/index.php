<?php
    require_once 'lib/funzioni.php';
    
    require_once 'config2.php';
    require_once 'lib/DbManager_mysqli.php';
 
	
	$username = '';
	$err = '';
	if(PostVal('sent')=='si')
	{
		$username = PostVal('username');
		$password = PostVal('password');
		$err = 'Credenziali errate';
		if($username=='admin' && $password=='pippo')
		{
			session_start();
			$_SESSION['autenticato'] = true;
			header('location:list.php');
			exit;
		}
	}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style> form{width:300px;height:200px;position:fixed;left:50%;top:50%;margin-left:-150px;margin-top:-100px;}
        span{color:#8bc34a;font-size:15px;display:block;margin:5px 0}
        input[type=text],input[type=password]{
        width: 90%;    
        border-top: none;
        border-left: none;
        border-right: none;
        margin-top: 10px;
        margin-bottom: 10px;
        display: block;
        padding-bottom: 5px;
        padding-top: 5px;
        transition: 0.3s;
        outline:none;
        font-size: 15px;
        }
        input[type=submit]{
            border: none;
            border-radius: 3px;
            background: #8bc34a;
            height: 35px;
            color: #fff;
            right: 10px;
            font-size: 14px;
            text-transform: uppercase;
            float: right;
            cursor: pointer;
        }
    </style>    
    </head>
    <body>
       <form action="index.php" method="post">
	   
            <span>Username</span>
            <input type="text" name="username" autocomplete="off" value="<?php echo(htmlentities($username)); ?>" /><br/><br/>
            <span>Password</span>
            <input type="password" name="password" autocomplete="off" value="" /><br/><br/>
            
            
            <input type="hidden" name="sent" value="si"/>
            <input type="submit" value="Log In"/>
            <p style="color:red;margin:0;"><?php echo($err); ?></p>
		</form>		
    </body>
</html>
