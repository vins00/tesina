<?php
    require_once 'lib/funzioni.php';
    
    if(extension_loaded('mysqli')){
        require_once 'lib/DbManager.php';
        require_once 'config.php';
    } else {
        require_once 'lib/DbManager_mysqli.php';
        require_once 'config2.php';
    };   
    require_once 'lib/authentication.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Lista Utenti</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
       <?php
        $dbMan = new DbManager(DB_HOST,DB_NAME,DB_USER,DB_PASSWORD);
        
        $sSQL = "SELECT * FROM products";
        
        $dbMan->Esegui($sSQL);  
        
        ?>
        <table>
        
<?php
        while($ret = $dbMan->Recupera($sSQL))
        {
?>   
            <tr> 
              <td>  
                <a href="modifica.php?id=<?php $ret['id'] ?>"> 
                <h5><?php $ret['title'] ?></h5>
                <span><?php $ret['image'] ?></span>
                <span><?php $ret['date'] ?></span>
                <span><?php $ret['price'] ?></span>
                </a>  
              </td>
              <td>
                <a href="delete.php?id=<?php $ret['id'] ?>">Elimina</a>  
              </td>
            </tr>
<?php    
        }
?>
        
        </table>
        
       <br/>
       <br/>
	   <a href="new.php">Nuovo articolo</a>
	   <a href="logout.php">Esci</a><br/>
    </body>
</html>
