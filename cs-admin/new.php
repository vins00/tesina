<?php
    require_once 'lib/funzioni.php';
    
    require_once 'config2.php';
    require_once 'lib/DbManager_mysqli.php';   
    require_once 'lib/authentication.php';

    $sTmp = '';
    if(PostVal('inviato')=='si')
    {
        $dbMan = new DbManager(DB_HOST,DB_NAME,DB_USER,DB_PASSWORD);
        
        $title = PostVal('titolo');
        $img = PostVal('img');
        $date = PostVal('uscita');
        $price = PostVal('prezzo');
        $desc = PostVal('desc');
        
        $title = addslashes($title);
        $img = addslashes($img);
        $date = addslashes($date);
        $price = addslashes($price);
        $desc = addslashes($desc);
 /*       
        $sel = PostVal('selection');
        
        $table = '';
        
        if($sel == 'ps4')
        {
            $table = 'ps4_products';
        }
        
        if($sel == 'one')
        {
            $table = 'xboxone_products';
        }
        
        if($sel == 'switch')
        {
            $table = 'switch_products';
        }
        
        if($sel == 'ps3')
        {
            $table = 'ps3_products';
        }
        
        if($sel == '360')
        {
            $table = 'xbox360_products';
        }
        
        if($sel == '3ds')
        {
            $table = '3ds_products';
        }
        
        if($sel == 'pc')
        {
            $table = 'pc_products';
        }
        
        if($sel == 'movie')
        {
            $table = 'movie_products';
        }
*/        
        $sSQL = "INSERT INTO products
                (
                        title
                        ,image
                        ,date
                        ,price
                        ,description
                )
                VALUES
                (
                        '$title'
                        ,'$img'
                        ,'$date'
                        ,'$price'
                        ,'$desc'
                )";
        
		
        $dbMan->Esegui($sSQL);
       
        
        //header("location:lista.php");    
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style> 
        form{width:400px;height:auto;margin-left:30px;margin-top:30px;}
        span{color:#8bc34a;font-size:15px;display:block;margin:5px 0;}
        input[type=text],input[type=password],input[type=number],textarea{
            width: 90%;    
            border-top: none;
            border-left: none;
            border-right: none;
            margin-top: 10px;
            margin-bottom: 10px;
            display: block;
            padding-bottom: 5px;
            padding-top: 5px;
            transition: 0.3s;
            outline:none;
            font-size: 15px;
            }
        input[type=number]{width:100px;display:inline;-webkit-appearance: none;}    
        input[type=submit]{
            border: none;
            border-radius: 3px;
            background: #8bc34a;
            height: 35px;
            color: #fff;
            right: 10px;
            font-size: 14px;
            text-transform: uppercase;
            float: right;
            cursor: pointer;
        }
        select{
            background-color: transparent;
            width: 100px;
            padding: 10px 10px 10px 0;
            font-size: 15px;
            border-radius: 0;
            border: none;
            border-bottom: 1px solid rgba(0,0,0, 0.12);
        }
        select:focus{outline: none;}    
        
    </style>
    </head>
    <body>
       <form class="" action="<?php echo($_SERVER['PHP_SELF']); ?>" method="post">
            <?php echo($sTmp) ?>

            <span class="">Titolo Articolo</span>
            <input class="" type="text" name="titolo" value="" /><br/><br/>
            <span class="">Immagine</span>
            <input class="" type="text" name="img" value="images/img/products/" /><br/><br/>
			<span class="">Data uscita</span>
            <input class="" type="text" name="uscita" value="" /><br/><br/>
            <span class="">Descrizione</span>
           <textarea class="" name="desc" value="" style="resize:none"></textarea><br/><br/>
            <span class="">Prezzo</span>
            <input class="" type="number" name="prezzo" value="00.00" /> €<br/><br/>
            <select name="selection">
                <option value="" disabled selected></option>
                <option value="ps4">ps4</option>
                <option value="one">xbox_one</option>
                <option value="switch">switch</option>
                <option value="ps3">ps3</option>
                <option value="360">xbox_360</option>
                <option value="3ds">3ds</option>
                <option value="pc">pc</option>
                <option value="movie">movie</option>
            </select>
            
            
            <input type="hidden" name="inviato" value="si"/>
            <input type="submit" value="Aggiungi"/>
	</form>	
      <br/>
       <br/>
       <a href="list.php">Vai alla lista</a>
	   <a href="logout.php">Esci</a><br/>	
    </body>
</html>
