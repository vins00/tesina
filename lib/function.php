<?php

	function PostVal($nome)
	{
		$valore='';
		if(isset($_POST[$nome]))
		{
			$valore = $_POST[$nome];
		} 
		return $valore;
	}
	
	function GetVal($nome)
	{
		$valore='';
		if(isset($_GET[$nome]))
		{
			$valore = $_GET[$nome];
		} 
		return $valore;
	}
