<?php
    $date = date('Y-m-d',time());

    $sSQL = "SELECT * FROM news WHERE date <= $date ORDER BY date ASC LIMIT 8";

    //$dbMan = new DbManager(DB_HOST,DB_NAME,DB_USER,DB_PASSWORD);
    //$DbMan -> Esegui($sSQL);

?>

<?php //NEWS ?>         

        <section id="newsSec" class="android-wear-section">
        <div id="newsContainer" class="android-news-section">
         <div class="android-section-title mdl-typography--display-1-color-contrast newsMainTitle">Ultime Notizie</div>
          <div class="android-card-container mdl-grid">
                      
            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card mdl newsCont1 pRel">
              <div class="mdl-card__media bigMedia borderR">
                <img src="images/img/news/bigNews.jpg">
              </div>
              <div class="bigTitle">
              <div class="mdl-card__title">
                 <h4 class="mdl-card__title-text">Destiny 2: I Rinnegati, Bungie è pronta a stravolgere tutto</h4>
              </div>
              <div class="mdl-card__supporting-text newsSubtitle">
                <span class="mdl-typography--font-light mdl-typography--subhead newsSubtitleSpan">Destiny 2: I Rinnegati porterà nuove minacce il 4 settembre e per affrontarle al meglio Bungie ha pensato di apportare significativi cambiamenti al sistema di equipaggiamento...
                </span>
              </div>
              <div class="mdl-card__actions">
                 <a class="android-link mdl-button mdl-js-button mdl-typography--text-uppercase" href="">
                   Scopri di più
                   <i class="material-icons">chevron_right</i>
                 </a>
              </div>
              </div>
              <span class="newsDate newsDateGreen">01 / 01</span>
            </div>
            
            <div class="smallNewsCont">
            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card newsCont2 pRel">
              <div class="mdl-card__media smallMedia">
                <img src="images/img/news/newsSmall1.jpg">
              </div>
              <div class="mdl-card__title smallTitle">
                 <a href=""><h4 class="mdl-card__title-text">Ubisoft Store: tanti giochi scontati</h4></a>
              </div>
              <span class="newsDate newsDateGreen">01 / 01</span>
            </div>
            
            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card newsCont3 pRel">
              <div class="mdl-card__media smallMedia">
                <img src="images/img/news/newsSmall2.jpg">
              </div>
              <div class="mdl-card__title smallTitle">
                 <a href=""><h4 class="mdl-card__title-text">Anthem non supporterà le mod su PC</h4></a>
              </div>
              <span class="newsDate newsDateGreen">01 / 01</span>
            </div>
            
            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card newsCont4 pRel">
              <div class="mdl-card__media smallMedia">
                <img src="images/img/news/newsSmall3.jpg">
              </div>
              <div class="mdl-card__title smallTitle">
                 <a href=""><h4 class="mdl-card__title-text">PS5 vedrà la luce nel 2019?</h4></a>
              </div>
              <span class="newsDate newsDateGreen">01 / 01</span>
            </div>
            </div>

            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card newsCont5 pRel">
              <div class="mdl-card__media borderR">
                <img src="images/img/news/news5.jpg">
              </div>
              <div class="mdl-card__title">
                 <h4 class="mdl-card__title-text">Pokemon Let's Go: il team Rocket e le Megaevoluzioni tra le novità</h4>
              </div>
              <div class="mdl-card__supporting-text">
                <span class="mdl-typography--font-light mdl-typography--subhead">In uscita il 16 novembre solo su Nintendo Switch, Pokémon: Let's Go, Pikachu! e Pokémon: Let's Go, Eevee! tornano ad ingolosire gli Allenatori con un nuovo trailer. Scopriamo tutte le novità!</span>
              </div>
              <div class="mdl-card__actions">
                 <a class="android-link mdl-button mdl-js-button mdl-typography--text-uppercase" href="">
                   Scopri di più
                   <i class="material-icons">chevron_right</i>
                 </a>
              </div>
              <span class="newsDate">01 / 01</span>
            </div>

            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card newsCont6 pRel">
              <div class="mdl-card__media borderR">
                <img src="images/img/news/news6.jpg">
              </div>
              <div class="mdl-card__title">
                 <h4 class="mdl-card__title-text">God Of War: disponibile il New Game+, introdotte numerose novità di gameplay</h4>
              </div>
              <div class="mdl-card__supporting-text">
                <span class="mdl-typography--font-light mdl-typography--subhead">Santa Monica ha quest'oggi pubblicato finalmente l'aggiornamento che introduce la modalità New Game+ all'interno di God of War, l'action-adventure disponibile su PlayStation 4...</span>
              </div>
              <div class="mdl-card__actions">
                 <a class="android-link mdl-button mdl-js-button mdl-typography--text-uppercase" href="">
                   Scopri di più
                   <i class="material-icons">chevron_right</i>
                 </a>
              </div>
              <span class="newsDate">01 / 01</span>
            </div>

            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card newsCont7 pRel">
              <div class="mdl-card__media borderR">
                <img src="images/img/news/news7.jpg">
              </div>
              <div class="mdl-card__title">
                 <h4 class="mdl-card__title-text">Avengers: Infinity War, un nuovo VFX reel ci mostra la creazione di una grande parte degli effetti visivi del film</h4>
              </div>
              <div class="mdl-card__supporting-text">
                <span class="mdl-typography--font-light mdl-typography--subhead">In rete è approdato un nuovo interessante video proveniente dal backstage di Avengers: Infinity War. Il video in questione, come gli ultimi arrivati...</span>
              </div>
              <div class="mdl-card__actions">
                 <a class="android-link mdl-button mdl-js-button mdl-typography--text-uppercase" href="">
                   Scopri di più
                   <i class="material-icons">chevron_right</i>
                 </a>
              </div>
              <span class="newsDate">01 / 01</span>
            </div>
            
            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card newsCont8 pRel">
              <div class="mdl-card__media borderR">
                <img src="images/img/news/news8.jpg">
              </div>
              
               <div class="mdl-card__title ">
                 <h4 class="mdl-card__title-text">Spyro Reignited Trilogy: ecco la probabile causa del rinvio</h4>
              </div>
               <div class="mdl-card__supporting-text">
                <span class="mdl-typography--font-light mdl-typography--subhead">
Stando a quanto dichiarato dallo youtuber YongYea, che sostiene di aver ricevuto l'informazione da una fonte affidabile, c'è un motivo...</span>
              </div>
                <div class="mdl-card__actions">
                 <a class="android-link mdl-button mdl-js-button mdl-typography--text-uppercase" href="">
                   Scopri di più
                   <i class="material-icons">chevron_right</i>
                 </a>
              </div>
              <span class="newsDate">01 / 01</span>
            </div>
            
          </div>
        </div>
        </section>
        