<?php
    require_once '../config2.php';
    require_once '../lib/DbManager_mysqli.php';
    require_once '../lib/function.php';
    
    date_default_timezone_set('Europe/Rome');
    $date = date('Y-m-d',time());

    $dbMan = new DbManager(DB_HOST,DB_NAME,DB_USER,DB_PASSWORD);

// SOON
    $sSQLsoon = "SELECT * FROM products WHERE date > '$date' AND xboxone = '1' ORDER BY date ASC LIMIT 5";
    $dbMan->Esegui($sSQLsoon);

    $html = '';
    $cc = 1;
    $class = 1;
    $span = '<span class="read-more-target">';
    
    $soonProd = [];
    
    while($ret = $dbMan->Recupera())
    { 
     $DATE = strtotime($ret['date']);        
     $html .= '<div class="soon'.$class.' android-screen pHover scale-transition scale-out mrgTop30">
                      <a class="android-image-link" href="#" data-toggle="modal" data-target="#modal_one'.$cc.'">
                       <div class="pRel">
                        <img class="android-screen-image mdl-shadow--3dp" src="images/img/products/one/'.$ret['image'].'">
                        <div class="overlay">
                        </a>
                            <a href="#"><div class="text"><i class="far fa-heart"></i></div></a>
                        </div>
                       </div>
                      <a class="android-link mdl-typography--font-regular mdl-typography--text-uppercase mdl-typography--text-left txtCtr" href="#" data-toggle="modal" data-target="#modal_one'.$cc.'"><br/>'.wordwrap($ret['title'],30,"<br/>").'<br/>'.date('d / m',$DATE).'</a>
                    </div>

                    <div class="modal fade" id="modal_one'.$cc.'"> 
                        <div class="modal-dialog" style="max-width:750px;">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title"><br/>'.strtoupper($ret['title']).'</h4>
                                    <span class="closeModal" data-dismiss="modal"><i class="fa fa-times"></i></span>
                                </div>
                                <div class="modal-body">
                                    <img class="modalImg" style="float:left;" src="images/img/products/one/'.$ret['image'].'" />    
                                    <h5 class="modal-desc">Descrizione</h5>
                                    <input type="checkbox" class="read-more-state" id="post-one-'.$cc.'" />
                                    <p class="modal-desc-text">'.substr_replace($ret['description'],$span,320,0).'</span></p>
                                    <label for="post-one-'.$cc.'" class="read-more-trigger"></label>
                                </div>
                                <div class="modal-footer">
                                    <span class="modalDate">'.date('d/m/y',$DATE).'</span>
                                    <button class="modalButton" type="button">Aggiungi alla wishlist <i class="far fa-heart"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>';
        
        $cc++;
        $class++;
    };

    $soonProd[] = utf8_encode($html);

// NEWS
    $sSQLnews = "SELECT * FROM products WHERE date < '$date' AND xboxone = '1' ORDER BY date DESC";
    $dbMan->Esegui($sSQLnews);

    $html2 ='';
    $newsProd = [];

    while($ret = $dbMan->Recupera())
    {         
     $html2 .= '<div class="soon1 android-screen pHover scale-transition scale-out mrgTop30">
                  <a class="android-image-link" href="#" data-toggle="modal" data-target="#modal_one'.$cc.'">
                   <div class="pRel">
                    <img class="android-screen-image mdl-shadow--3dp" src="images/img/products/one/'.$ret['image'].'">
                    <div class="overlay">
                    </a>
                        <a href="#"><div class="text"><i class="far fa-heart"></i></div></a>
                    </div>
                   </div>
                  <a class="android-link mdl-typography--font-regular mdl-typography--text-uppercase mdl-typography--text-left txtCtr" href="#" data-toggle="modal" data-target="#modal_one'.$cc.'"><br/>'.wordwrap($ret['title'],30,"<br/>").'<br/>'.$ret['price'].'&euro;</a>
                </div>

                <div class="modal fade" id="modal_one'.$cc.'"> 
                    <div class="modal-dialog" style="max-width:750px;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><br/>'.strtoupper($ret['title']).'</h4>
                                <span class="closeModal" data-dismiss="modal"><i class="fa fa-times"></i></span>
                            </div>
                            <div class="modal-body">
                                <img class="modalImg" style="float:left;" src="images/img/products/one/'.$ret['image'].'" />    
                                <h5 class="modal-desc">Descrizione</h5>
                                <input type="checkbox" class="read-more-state" id="post-one-'.$cc.'" />
                                <p class="modal-desc-text">'.substr_replace($ret['description'],$span,320,0).'</span></p>
                                <label for="post-one-'.$cc.'" class="read-more-trigger"></label>
                            </div>
                            <div class="modal-footer">
                                <span class="modalDate">'.$ret['price'].'&euro;</span>
                                <button class="modalButton" type="button">Aggiungi alla wishlist <i class="far fa-heart"></i></button>
                            </div>
                        </div>
                    </div>
                </div>';
        
        $cc++;
    };

    $newsProd[] = utf8_encode($html2);

// PROMO
    $sSQLpromo = "SELECT * FROM products WHERE date < '$date' AND xboxone = '1' AND price_disc IS NOT NULL ORDER BY date DESC";
    $dbMan->Esegui($sSQLpromo);

    $html3 ='';
    $promoProd = [];

    while($ret = $dbMan->Recupera())
    {         
     $html3 .= '<div class="soon1 android-screen pHover scale-transition scale-out mrgTop30">
                  <a class="android-image-link" href="#" data-toggle="modal" data-target="#modal_one'.$cc.'">
                   <div class="pRel">
                    <img class="android-screen-image mdl-shadow--3dp" src="images/img/products/one/'.$ret['image'].'">
                    <div class="overlay">
                    </a>
                        <a href="#"><div class="text"><i class="far fa-heart"></i></div></a>
                    </div>
                   </div>
                  <a class="android-link mdl-typography--font-regular mdl-typography--text-uppercase mdl-typography--text-left txtCtr" href="#" data-toggle="modal" data-target="#modal_one'.$cc.'"><br/>'.wordwrap($ret['title'],30,"<br/>").'<br/><strike>'.$ret['price'].'&euro;</strike>&nbsp;&nbsp;'.$ret['price_disc'].'&euro;</a>
                </div>

                <div class="modal fade" id="modal_one'.$cc.'"> 
                    <div class="modal-dialog" style="max-width:750px;">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><br/>'.strtoupper($ret['title']).'</h4>
                                <span class="closeModal" data-dismiss="modal"><i class="fa fa-times"></i></span>
                            </div>
                            <div class="modal-body">
                                <img class="modalImg" style="float:left;" src="images/img/products/one/'.$ret['image'].'" />    
                                <h5 class="modal-desc">Descrizione</h5>
                                <input type="checkbox" class="read-more-state" id="post-one-'.$cc.'" />
                                <p class="modal-desc-text">'.substr_replace($ret['description'],$span,320,0).'</span></p>
                                <label for="post-one-'.$cc.'" class="read-more-trigger"></label>
                            </div>
                            <div class="modal-footer">
                                <span class="modalDate"><strike>'.$ret['price'].'&euro;</strike>&nbsp;&nbsp;'.$ret['price_disc'].'&euro;</span>
                                <button class="modalButton" type="button">Aggiungi alla wishlist <i class="far fa-heart"></i></button>
                            </div>
                        </div>
                    </div>
                </div>';
        
        $cc++;
    };

    $promoProd[] = utf8_encode($html3);

    $arOut = ['soon' => $soonProd, 'news' => $newsProd, 'promo' => $promoProd];
    $json = json_encode($arOut);
    header('content-type:application/json');
    http_response_code(200);

    if(json_last_error()!=JSON_ERROR_NONE)
    {

        die(json_last_error_msg());
    }

    echo($json);

