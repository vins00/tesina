
<?php //INFO ?>
       
        <section id="infoSec" class="android-more-section">
            <div class="android-section-title mdl-typography--display-1-color-contrast">Info &amp; Orari</div>
            <p class="mdl-typography--headline mdl-typography--font-thin fntSz16 mrgLft12">
                VIA R. GIULIANI, 70/C ROSSO, 50141 FIRENZE FI · 055 428 9002 · <a class="mailSpan" href="#contact">@CONSOLESTATIONSNC</a>
            </p>
            <table class="striped">
            <tbody>
                  <tr>
                    <td class="pdgTop10 pdgBtm10 fntB500">LUNEDÌ</td>
                    <td>Chiuso</td>
                    <td>15.30 - 20</td>
                  </tr>
                  <tr>
                    <td  class="pdgTop10 pdgBtm10 fntB500">MARTEDÌ - SABATO</td>
                    <td>10 - 13</td>
                    <td>15.30 - 20</td>
                  </tr>
                  <tr>
                    <td  class="pdgTop10 pdgBtm10 fntB500">DOMENICA</td>
                    <td>Chiuso</td>
                    <td> </td>
                  </tr>
                </tbody>
           </table>
           <div class="mapGoogle">
           <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23041.684495808746!2d11.233637971187557!3d43.789243643837736!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132a568faf29c739%3A0xfabeedef4bac2bd4!2sConsole+Station+Snc!5e0!3m2!1sit!2sit!4v1535056066209" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
           </div>
           
        </section>
