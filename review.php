
<?php //REVIEW ?>      
       
        <section id="reviewSec" class="">
         <div id="reviewCont" class="android-more-section">
          <div class="android-section-title mdl-typography--display-1-color-contrast">In Primo Piano</div>
          <div class="android-card-container mdl-grid">
            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card mdl-shadow--3dp">
              <div class="mdl-card__media">
                <img src="images/img/review/top1.jpg">
              </div>
              <div class="mdl-card__title">
                 <h4 class="mdl-card__title-text">Detroit Become Human</h4>
              </div>
              <div class="mdl-card__supporting-text">
                <span class="mdl-typography--font-light mdl-typography--subhead">Scrivi il futuro. Immergiti in una visione inquieta della Detroit del futuro, dove umani e androidi conducono una fragile convivenza e le tue decisioni costruiscono la storia che ti circonda.
                </span>
              </div>
              <div class="mdl-card__actions">
                 <a class="android-link mdl-button mdl-js-button mdl-typography--text-uppercase" href="">
                   Recensione completa
                   <i class="material-icons">chevron_right</i>
                 </a>
              </div>
            </div>

            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card mdl-shadow--3dp">
              <div class="mdl-card__media">
                <img src="images/img/review/top2.jpg">
              </div>
              <div class="mdl-card__title">
                 <h4 class="mdl-card__title-text">God of War</h4>
              </div>
              <div class="mdl-card__supporting-text">
                <span class="mdl-typography--font-light mdl-typography--subhead">Lasciatosi alle spalle la sua sete di vendetta verso gli dèi dell'Olimpo, Kratos dovrà combattere per la sopravvivenza e insegnare a suo figlio non solo a fare lo stesso, ma anche a evitare di ripetere gli stessi errori fatali del Fantasma di Sparta...</span>
              </div>
              <div class="mdl-card__actions">
                 <a class="android-link mdl-button mdl-js-button mdl-typography--text-uppercase" href="">
                   Recensione completa
                   <i class="material-icons">chevron_right</i>
                 </a>
              </div>
            </div>

            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card mdl-shadow--3dp">
              <div class="mdl-card__media">
                <img src="images/img/review/top3.jpg">
              </div>
              <div class="mdl-card__title">
                 <h4 class="mdl-card__title-text">Far Cry 5</h4>
              </div>
              <div class="mdl-card__supporting-text">
                <span class="mdl-typography--font-light mdl-typography--subhead">Ecco Hope County, Montana, terra dei liberi, patria dei coraggiosi... e della setta apocalittica Eden’s Gate. Affronta il capo spirituale Joseph Seed e i suoi fratelli, gli Araldi: accendi il fuoco della ribellione e libera una comunità sotto assedio.</span>
              </div>
              <div class="mdl-card__actions">
                 <a class="android-link mdl-button mdl-js-button mdl-typography--text-uppercase" href="">
                   Recensione completa
                   <i class="material-icons">chevron_right</i>
                 </a>
              </div>
            </div>

            <div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--4-col-phone mdl-card mdl-shadow--3dp">
              <div class="mdl-card__media">
                <img src="images/img/review/top4.jpg">
              </div>
              <div class="mdl-card__title">
                 <h4 class="mdl-card__title-text">Tomb Raider</h4>
              </div>
              <div class="mdl-card__supporting-text">
                <span class="mdl-typography--font-light mdl-typography--subhead">Determinata nel trovare la sua strada, Lara si rifiuta di assumere il comando dell’impero globale del padre, così come rifiuta fermamente l’idea che lui sia veramente scomparso.</span>
              </div>
              <div class="mdl-card__actions">
                 <a class="android-link mdl-button mdl-js-button mdl-typography--text-uppercase" href="">
                   Recensione completa
                   <i class="material-icons">chevron_right</i>
                 </a>
              </div>
            </div>
          </div>
          </div>
        </section>
        