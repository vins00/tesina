<script src="js/dbProducts/ajax.js"></script>

<?php //PRODUCT ?>
       
        <section id="productSec" aria-atomic="" class="android-screen-section mdl-typography--text-center mrgTop50 wdt100vw">
          <div class="mdl-typography--display-1-color-contrast">Ultime Novità</div>
          <hr class="titleLine" />
          <p class="mdl-typography--font-light">
              Scopri i Nuovi Arrivi e <a href="#contact" class="android-link mdl-typography--font-light">Verifica la Disponibilità in Negozio</a>
          </p>
          <div id="productCont" class="">
             <p class="floatLft">
                  <input id="bALL" name="group2" type="radio" value="ALL"  onclick="getProducts()" checked="checked" />
                  <label for="bALL" class="soonLabel mdl-shadow--3dp">
                    <i class="fas fa-gamepad consoleIconCurr"></i>
                    <span>TUTTE</span>
                  </label>
              </p>
              <p class="floatLft">
                  <input id="bPS4" name="group2" type="radio" value="PS4"  onclick="getProducts()" />
                  <label for="bPS4" class="soonLabel mdl-shadow--3dp">
                    <i class="fab fa-playstation"></i>
                    <span>PS4</span>
                  </label>
              </p>
              <p class="floatLft">
                  <input id="bONE" name="group2" type="radio" value="ONE"  onclick="getProducts()" />
                  <label for="bONE" class="soonLabel mdl-shadow--3dp">
                    <i class="fab fa-xbox"></i>
                    <span>XBOXONE</span>
                  </label>
              </p>
              <p class="floatLft">
                  <input id="bSWITCH" name="group2" type="radio" value="SWITCH"  onclick="getProducts()" />
                  <label for="bSWITCH" class="soonLabel mdl-shadow--3dp">    
                    <i class="fab fa-nintendo-switch"></i>
                    <span>SWITCH</span>
                  </label>
              </p>
<!--              
              <p class="floatLft">
                  <input id="bPS3" name="group2" type="radio" value="PS3" onchange="cnsVal()" />
                  <label for="bPS3" class="soonLabel mdl-shadow--3dp">    
                    <i class="fab fa-playstation"></i>
                    <span>PS3</span>
                  </label>
              </p>
              <p class="floatLft">
                  <input id="b360" name="group2" type="radio" value="360" onchange="cnsVal()" />
                  <label for="b360" class="soonLabel mdl-shadow--3dp">   
                    <i class="fab fa-xbox"></i>
                    <span>XBOX360</span>
                  </label>
              </p>
-->              
              <p class="floatLft">
                  <input id="b3DS" name="group2" type="radio" value="3DS"  onclick="getProducts()" />
                  <label for="b3DS" class="soonLabel mdl-shadow--3dp">    
                    <i class="fab fa-nintendo-switch"></i>
                    <span>3DS</span>
                  </label>
              </p>
              <p class="floatLft">
                  <input id="bPC" name="group2" type="radio" value="PC"  onclick="getProducts()" />
                  <label for="bPC" class="soonLabel mdl-shadow--3dp">    
                    <i class="fab fa-windows"></i>
                    <span>PC</span>
                  </label>
              </p>
              <p class="floatLft">
                  <input id="bMovie" name="group2" type="radio" value="MOVIE"  onclick="getProducts()" />
                  <label for="bMovie" class="soonLabel mdl-shadow--3dp">    
                    <i class="fas fa-film"></i>
                    <span>DVD/BLURAY</span>
                  </label>
              </p>
          </div>
          <div id="chips" class="">
              <p class="floatLft">
                  <input id="bNews" name="group3" type="radio" value="news" checked="checked" />
                  <label for="bNews" class="chipsClass">
                    <span>Novità</span>
                  </label>
              </p>
              <p class="floatLft">
                  <input id="bSoon" name="group3" type="radio" value="soon" />
                  <label for="bSoon" class="chipsClass">    
                    <span>In Uscita</span>
                  </label>
              </p>
              <p class="floatLft">
                  <input id="bPromo" name="group3" type="radio" value="promo" />
                  <label for="bPromo" class="chipsClass">    
                    <span>Promo</span>
                  </label>
              </p>
              <p class="floatLft">
                  <input type="text" id="productSearch" placeholder="Cerca"/>
                  <label for="productSearch"><i class="material-icons">search</i></label>
              </p>
          </div>
<!-- ALL -->         
          <div id="contALL" class="android-screens mrgTop200 ALL cont">
              <div id="ALLsoon" class="android-screens SH"></div>          
              <div id="ALLnews" class="android-screens SH"></div>
              <div id="ALLpromo" class="android-screens SH"></div>
          </div>
<!-- PS4 -->         
          <div id="contPS4" class="android-screens mrgTop200 PS4 cont">
              <div id="PS4soon" class="android-screens SH"></div>          
              <div id="PS4news" class="android-screens SH"></div>
              <div id="PS4promo" class="android-screens SH"></div>
          </div>
<!-- ONE -->         
          <div id="contONE" class="android-screens mrgTop200 ONE cont">
              <div id="ONEsoon" class="android-screens SH"></div>          
              <div id="ONEnews" class="android-screens SH"></div>
              <div id="ONEpromo" class="android-screens SH"></div>
          </div>
<!-- SWITCH -->         
          <div id="contSWITCH" class="android-screens mrgTop200 SWITCH cont">
              <div id="SWITCHsoon" class="android-screens SH"></div>          
              <div id="SWITCHnews" class="android-screens SH"></div>
              <div id="SWITCHpromo" class="android-screens SH"></div>
          </div> 
<!-- 3DS -->         
          <div id="cont3DS" class="android-screens mrgTop200 3DS cont">
              <div id="3DSsoon" class="android-screens SH"></div>          
              <div id="3DSnews" class="android-screens SH"></div>
              <div id="3DSpromo" class="android-screens SH"></div>
          </div>
<!-- PC -->         
          <div id="contPC" class="android-screens mrgTop200 PC cont">
              <div id="PCsoon" class="android-screens SH"></div>          
              <div id="PCnews" class="android-screens SH"></div>
              <div id="PCpromo" class="android-screens SH"></div>
          </div> 
<!-- MOVIE -->         
          <div id="contMOVIE" class="android-screens mrgTop200 MOVIE cont">
              <div id="MOVIEsoon" class="android-screens SH"></div>          
              <div id="MOVIEnews" class="android-screens SH"></div>
              <div id="MOVIEpromo" class="android-screens SH"></div>
          </div>                                                
          
          <ul class="pagination center-block">
            <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
            <li class="active"><a href="#!">1</a></li>
            <li class="waves-effect"><a href="#!">2</a></li>
            <li class="waves-effect"><a href="#!">3</a></li>
            <li class="waves-effect"><a href="#!">4</a></li>
            <li class="waves-effect"><a href="#!">5</a></li>
            <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
          </ul>
    
        </section>
        
        