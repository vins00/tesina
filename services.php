
<?php //SERVICES ?> 
       
        <section id="servicesSec" class="android-customized-section">
          <div class="android-customized-section-text mrgBtm40">
            <div class="mdl-typography--font-light mdl-typography--display-1-color-contrast">Una risposta ad ogni tua esigenza</div>
            <hr class="titleLine" />
            <p class="mdl-typography--font-light">
              Offriamo una vasta gamma di servizi e prodotti per coltivare la tua  passione per il mondo dei videogiochi 
              <br>
              <a href="#product" class="android-link mdl-typography--font-light">Visualizza i nostri prodotti</a>
            </p>
          </div>
          <div class="android-customized-section-image">
              <div class="row">
              <div class="col-lg-8 my-auto">
                <div class="container-fluid">
                  <div class="row">
                    <div id="feat1" class="col-lg-6 feature-cln floatLft colWdt">
                      <div class="feature-item mrgFeatures">
                        <i class="fa fa-gamepad"></i>
                        <h3>Day One</h3>
                        <p class="text-muted">Prenota e riserva subito la copia del tuo gioco preferito</p>
                      </div>
                    </div>
                    <div id="feat2" class="col-lg-6 feature-cln floatLft colWdt">
                      <div class="feature-item mrgFeatures">
                        <i class="fa fa-hands-helping"></i>
                        <h3>Usato</h3>
                        <p class="text-muted">Portaci i tuoi giochi usati e ricevi sconti sui nuovi prodotti</p>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div id="feat3" class="col-lg-6 feature-cln floatLft colWdt">
                      <div class="feature-item">
                        <i class="fa fa-chess"></i>
                        <h3>Prodotti</h3>
                        <p class="text-muted">Offriamo un'ampia varietà di prodotti tra cui videogiochi, film, action figures, gadget e altro ancora</p>
                      </div>
                    </div>
                    <div id="feat4" class="col-lg-6 feature-cln floatLft colWdt">
                      <div class="feature-item">
                        <i class="fa fa-gift"></i>
                        <h3>Offerte</h3>
                        <p class="text-muted">Ritira la tua carta socio e ricevi offerte sui tanti prodotti</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        